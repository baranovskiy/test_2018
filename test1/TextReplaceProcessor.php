<?php

declare(strict_types=1);

class TextReplaceProcessor
{
    private const BATCH_SIZE = 8192;

    /**
    * @var string
    */
    private $sourceFilePath;

    /**
    * @var string
    */
    private $outputFilePath;

    /**
    * @var array
    */
    private $config;

    /**
    * @var \SplFileObject
    */
    private $sourceFileObject;

    /**
    * @var \SplFileObject
    */
    private $outputFileObject;

    /**
    * @var int
    */
    private $lastWordPosition = 1;

    /**
     * @param string $sourceFilePath
     * @param string $outputFilePath
     * @param array $config
     */
    public function __construct(string $sourceFilePath, string $outputFilePath, array $config)
    {
        $this->sourceFilePath = $sourceFilePath;
        $this->outputFilePath = $outputFilePath;
        $this->config = $config;
    }

    public function process()
    {
        $this->loadSourceFile();
        $this->createOutputFile();

        while (false === $this->sourceFileObject->eof()) {
            $words = explode(' ', $this->sourceFileObject->fread(self::BATCH_SIZE));

            $this->appendToOutputFile($this->getReplacedChunk($words));

            $this->lastWordPosition += count($words);
        }
    }

    public function reset()
    {
        $this->sourceFileObject = null;
        $this->outputFileObject = null;
        $this->config = null;

        $this->lastWordPosition = 0;
    }

    private function createOutputFile()
    {
        $this->outputFileObject = new \SplFileObject($this->outputFilePath, 'w+');
    }

    private function appendToOutputFile(string $chunk)
    {
        $this->outputFileObject->fwrite($chunk);
    }

    /**
     * @throws Exception
     */
    private function loadSourceFile()
    {
        if (!file_exists($this->sourceFilePath)) {
            throw new Exception(sprintf('File %s doesn\'t exist', $this->sourceFilePath));
        }

        $this->sourceFileObject = new \SplFileObject($this->sourceFilePath);
    }

    /**
     * @param array $words
     *
     * @return string
     */
    private function getReplacedChunk(array $words): string
    {
        $resultContent = [];

        foreach ($words as $position => $word) {
            $resultContent[] = $this->getReplacedWord($position) ?: $word;
        }

        return implode(' ', $resultContent);
    }

    /**
     * @param int $position
     *
     * @return null|string
     */
    private function getReplacedWord(int $position): ?string
    {
        foreach ($this->config as $newWord => $value) {
            if (($position + $this->lastWordPosition) % $value === 0) {
                return $newWord;
            }
        }

        return null;
    }
}
