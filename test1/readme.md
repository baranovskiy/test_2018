
**How to use:**

$config = [
    '-ПЯТНАДЦАТЬ-' => 15,
    '-ТРИ-' => 3,
    '-ПЯТЬ-' => 5,
];

$file = new TextReplaceProcessor('text.txt', 'result.txt', $config);

$file->process();


Time spent: ~ 1h 30m