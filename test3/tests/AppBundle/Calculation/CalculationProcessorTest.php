<?php

namespace BotBundle\Tests\Controller;

use AppBundle\Calculation\CalculationProcessor;
use AppBundle\Entity\Calculation;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CalculationProcessorTest extends WebTestCase
{
    /**
     * @var CalculationProcessor
     */
    private $processor;

    public function setUp()
    {
        $this->processor = new CalculationProcessor($this->getCalculationMock());
        $this->processor->calculate();
    }

    public function testPaymentValue()
    {
        $lines = $this->processor->getCalculationLines();

        foreach ($lines as $calculationLine) {
            $this->assertSame($calculationLine->getPaymentValue(), 119.27702938066);
        }
    }

    public function testDebt()
    {
        $lines = $this->processor->getCalculationLines();

        $this->assertSame($lines[5]->getDebtValue(), 59.63851469033179);
        $this->assertSame($lines[8]->getDebtValue(), 29.81925734516589);
    }

    public function testTotalDebt()
    {
        $lines = $this->processor->getCalculationLines();

        $this->assertSame($lines[3]->getTotalDebtValue(), 954.2162350453086);
        $this->assertSame($lines[6]->getTotalDebtValue(), 596.385146903318);
    }

    /**
     * @return \PHPUnit\Framework\MockObject\Builder\InvocationMocker|Calculation
     * @throws \ReflectionException
     */
    private function getCalculationMock()
    {
        $mock = $this->createMock(Calculation::class);

        $mock->method('getCreditTerm')
            ->willReturn(12);

        $mock->method('getSumValue')
            ->willReturn(1000);

        $mock->method('getRatePercentage')
            ->willReturn(6 / 100);

        $mock->method('getFirstCreditPaymentDate')
            ->willReturn(new \DateTime());

        return $mock;
    }
}
