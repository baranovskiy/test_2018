$(document).ready(function () {
    $('#calculationForm').submit(function (e) {
        e.preventDefault();
        $.post('/calculate', $(this).serialize(), function (response) {
            if (!response.hasOwnProperty('error')) {
                renderResultTable(response);
            } else {
                alert('Something went wrong...');
            }
        })
    });
    
    function renderResultTable(response) {
        var $table = $('.table')

        $table.find('tbody tr').remove();
        $table.show();

        for (var key in response) {
            var $tr = $('<tr>');

            var $number = $('<td>').text(key),
                $month = $('<td>').text(response[key].month),
                $paymentDate = $('<td>').text(response[key].paymentDate),
                $totalDebt = $('<td>').text(response[key].totalDebtValue),
                $debt = $('<td>').text(response[key].debtValue),
                $paymentValue = $('<td>').text(response[key].paymentValue);

            $tr.append($number).append($month).append($paymentDate).append($totalDebt).append($debt).append($paymentValue);

            $table.find('tbody').append($tr);
        }
    }
});