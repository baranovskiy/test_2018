<?php

namespace AppBundle\Form;

use AppBundle\Entity\Calculation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CalculationFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('creditTerm', NumberType::class, [
                'required' => true,
                'label' => 'Choose a credit term in months.',
            ])
            ->add('ratePercentage', NumberType::class, [
                'required' => false,
                'label' => 'Choose a yearly percentage rate.',
            ])
            ->add('sumValue', NumberType::class, [
                'required' => true,
                'label' => 'Input sum value.',
            ])
            ->add('firstCreditPaymentDate', DateType::class, [
                'required' => true,
                'label' => 'Input first payment date.',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Calculate!',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Calculation::class,
            'method' => 'post',
        ]);
    }
}
