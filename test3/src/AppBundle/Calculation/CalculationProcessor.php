<?php

namespace AppBundle\Calculation;

use AppBundle\Entity\Calculation;
use AppBundle\Entity\CalculationLine;

class CalculationProcessor
{
    /**
     * @var Calculation
     */
    private $calculation;

    /**
     * @var float
     */
    private $annuityRatio;

    /**
     * @var float
     */
    private $monthlyValue;

    /**
     * @var float
     */
    private $totalValue;

    /**
     * @var Calculation[]
     */
    private $calculationLines = [];

    /**
     * @param Calculation $calculation
     */
    public function __construct(Calculation $calculation)
    {
        $this->calculation = $calculation;
    }

    public function calculate()
    {
        $this->calculateAnnuityRation();
        $this->calculateMonthlyValue();
        $this->calculateTotalValue();

        $this->generateCalculationDetails();
    }

    private function generateCalculationDetails()
    {
        foreach (range(0, $this->calculation->getCreditTerm() - 1) as $monthNum) {
            $calculationDetail = new CalculationLine($this->calculation);

            $calculationDetail->setPaymentDate($monthNum);
            $calculationDetail->setMonthNumber($monthNum + 1);
            $calculationDetail->setTotalDebtValue($this->totalValue - ($monthNum + 1) * $this->monthlyValue);
            $calculationDetail->setPaymentValue($this->monthlyValue);

            $calculationDetail->calculateDebtValue();

            $this->calculationLines[] = $calculationDetail;
        }
    }

    /**
     * @return CalculationLine[]
     */
    public function getCalculationLines(): array
    {
        return $this->calculationLines;
    }

    private function calculateMonthlyValue()
    {
        $this->monthlyValue = $this->calculation->getSumValue() * $this->annuityRatio;
    }

    private function calculateTotalValue()
    {
        $this->totalValue = $this->monthlyValue * $this->calculation->getCreditTerm();
    }

    private function calculateAnnuityRation()
    {
        $iN = pow(1 + $this->calculation->getRatePercentage(), $this->calculation->getCreditTerm());

        $this->annuityRatio = ($this->calculation->getRatePercentage() *  $iN) / ($iN - 1);
    }
}
