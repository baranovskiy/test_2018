<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CalculationDetail
 *
 * @ORM\Table(name="tblCalculationLine")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CalculationLineRepository")
 */
class CalculationLine
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Calculation
     *
     * @ORM\ManyToOne(targetEntity="Calculation")
     * @ORM\JoinColumn(name="calculationId", referencedColumnName="id")
     */
    private $calculation;

    /**
     * @var int
     *
     * @ORM\Column(name="monthNumber", type="integer")
     */
    private $monthNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="paymentDate", type="datetime")
     */
    private $paymentDate;

    /**
     * @var float
     *
     * @ORM\Column(name="debtValue", type="float")
     */
    private $debtValue;

    /**
     * @var float
     *
     * @ORM\Column(name="totalDebtValue", type="float")
     */
    private $totalDebtValue;

    /**
     * @var float
     *
     * @ORM\Column(name="paymentValue", type="float")
     */
    private $paymentValue;

    /**
     * @param Calculation $calculation
     */
    public function __construct(Calculation $calculation)
    {
        $this->calculation = $calculation;
    }

    /**
     * @param int $monthNumber
     */
    public function setPaymentDate(int $monthNumber)
    {
        $date = clone $this->calculation->getFirstCreditPaymentDate();
        $this->paymentDate = $date->modify(sprintf('+%d months', $monthNumber));
    }

    /**
     * @param int $monthNumber
     */
    public function setMonthNumber(int $monthNumber): void
    {
        $this->monthNumber = $monthNumber;
    }

    /**
     * @param float $totalDebtValue
     */
    public function setTotalDebtValue(float $totalDebtValue): void
    {
        $this->totalDebtValue = $totalDebtValue;
    }

    public function calculateDebtValue(): void
    {
        $this->debtValue = $this->totalDebtValue / 12;
    }

    /**
     * @param float $paymentValue
     */
    public function setPaymentValue(float $paymentValue): void
    {
        $this->paymentValue = $paymentValue;
    }

    /**
     * @return \DateTime
     */
    public function getPaymentDate(): \DateTime
    {
        return $this->paymentDate;
    }

    /**
     * @return float
     */
    public function getDebtValue(): float
    {
        return $this->debtValue;
    }

    /**
     * @return int
     */
    public function getMonthNumber(): int
    {
        return $this->monthNumber;
    }

    /**
     * @return float
     */
    public function getPaymentValue(): float
    {
        return $this->paymentValue;
    }

    /**
     * @return float
     */
    public function getTotalDebtValue(): float
    {
        return $this->totalDebtValue;
    }
}

