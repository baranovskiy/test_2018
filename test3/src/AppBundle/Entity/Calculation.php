<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CalculationHistory
 *
 * @ORM\Table(name="tblCalculation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CalculationRepository")
 */
class Calculation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="creditTerm", type="integer")
     */
    private $creditTerm;

    /**
     * @var float
     *
     * @ORM\Column(name="ratePercentage", type="float")
     */
    private $ratePercentage;

    /**
     * @var float
     *
     * @ORM\Column(name="sumValue", type="float")
     */
    private $sumValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="firstCreditPaymentDate", type="datetime", nullable=true)
     */
    private $firstCreditPaymentDate;

    /**
     * @var CalculationLine[]
     *
     * @ORM\OneToMany(targetEntity="CalculationLine", mappedBy="calculation")
     */
    private $calculationLines;

    public function __construct()
    {
        $this->calculationLines = new ArrayCollection();
    }

    /**
     * @param int $creditTerm
     */
    public function setCreditTerm(int $creditTerm): void
    {
        $this->creditTerm = $creditTerm;
    }

    /**
     * @param \DateTime $firstCreditPaymentDate
     */
    public function setFirstCreditPaymentDate(\DateTime $firstCreditPaymentDate): void
    {
        $this->firstCreditPaymentDate = $firstCreditPaymentDate;
    }

    /**
     * @param float $ratePercentage
     */
    public function setRatePercentage(float $ratePercentage): void
    {
        $this->ratePercentage = $ratePercentage / 100;
    }

    /**
     * @param float $sumValue
     */
    public function setSumValue(float $sumValue): void
    {
        $this->sumValue = $sumValue;
    }

    /**
     * @param CalculationLine $calculationLine
     */
    public function addLine(CalculationLine $calculationLine)
    {
        $this->calculationLines->add($calculationLine);
    }

    /**
     * @return int
     */
    public function getCreditTerm(): ?int
    {
        return $this->creditTerm;
    }

    /**
     * @return \DateTime
     */
    public function getFirstCreditPaymentDate(): ?\DateTime
    {
        return $this->firstCreditPaymentDate;
    }

    /**
     * @return float
     */
    public function getRatePercentage(): ?float
    {
        return $this->ratePercentage;
    }

    /**
     * @return float
     */
    public function getSumValue(): ?float
    {
        return $this->sumValue;
    }

    /**
     * @return string
     */
    public function getCalculationNumber(): string
    {
        return sprintf('#%d', $this->id);
    }

    /**
     * @return CalculationLine[]
     */
    public function getCalculationLines()
    {
        return $this->calculationLines;
    }
}

