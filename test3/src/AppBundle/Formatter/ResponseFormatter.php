<?php

namespace AppBundle\Formatter;

use AppBundle\Entity\Calculation;

class ResponseFormatter
{
    /**
     * @param Calculation $calculation
     *
     * @return array
     */
    public static function formatCalculationResult(Calculation $calculation): array
    {
        $result = [];

        foreach ($calculation->getCalculationLines() as $calculationLine) {
            $result[] = [
                'month' => $calculationLine->getMonthNumber(),
                'paymentDate' => $calculationLine->getPaymentDate()->format('Y-m-d'),
                'debtValue' => $calculationLine->getDebtValue(),
                'totalDebtValue' => $calculationLine->getTotalDebtValue(),
                'paymentValue' => $calculationLine->getPaymentValue(),
            ];
        }

        return $result;
    }
}
