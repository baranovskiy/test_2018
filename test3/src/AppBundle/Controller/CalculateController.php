<?php

namespace AppBundle\Controller;

use AppBundle\Calculation\CalculationProcessor;
use AppBundle\Form\CalculationFormType;
use AppBundle\Formatter\ResponseFormatter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CalculateController extends Controller
{
    /**
     * @return Response
     */
    public function indexAction(): Response
    {
        $calculationForm = $this->createForm(CalculationFormType::class);

        return $this->render('@App/default/index.html.twig', ['form' => $calculationForm->createView()]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function calculateAction(Request $request): JsonResponse
    {
        $form = $this->createForm(CalculationFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $calculation = $form->getData();
            $this->getDoctrine()->getManager()->persist($calculation);

            $calculationProcessor = new CalculationProcessor($calculation);
            $calculationProcessor->calculate();

            foreach ($calculationProcessor->getCalculationLines() as $calculationLine) {
                $calculation->addLine($calculationLine);
                $this->getDoctrine()->getManager()->persist($calculationLine);
            }

            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(ResponseFormatter::formatCalculationResult($calculation));
        }

        return new JsonResponse(['error' => true]);
    }
}
